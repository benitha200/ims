<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\SparePartsSearch;
use yii\data\SqlDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel=new SparePartsSearch();
        $dataProviderSparePart = $searchModel->search($this->request->queryParams);
        $dataProviderSparePart->query->orderBy(['quantity'=>SORT_ASC]);

        $sql="select spare_part,sum((price*quantity)-((price*quantity*discount)/100)) as Amount from retails_info  group by spare_part order by sum((price*quantity)-((price*quantity*discount)/100)) DESC limit 5";
        $count = Yii::$app->db->createCommand("SELECT COUNT(*) FROM ($sql) t")->queryScalar();

        $dataProvider = new SqlDataProvider([
            'sql' => $sql,
            'totalCount' => $count,
            'pagination' => [
                'pageSize' => false,
            ],
           
           
        ]);

        $sql2="select day(date(from_unixTime(created_at))) as _date,sum((price*quantity)-((price*quantity*discount)/100)) as Amount from retails_info WHERE month(date(from_unixTime(created_at)))=month(curdate()) group by day(date(from_unixTime(created_at)))";
        $count2 = Yii::$app->db->createCommand("SELECT COUNT(*) FROM ($sql2) t")->queryScalar();

        $dataProvider2 = new SqlDataProvider([
            'sql' => $sql2,
            'totalCount' => $count2,
            'pagination' => [
                'pageSize' => false,
            ],
           // 'pageSize'=>1,
           
        ]);
        return $this->render('index',[
            'dataProvider'=>$dataProvider,
            'dataProvider2'=>$dataProvider2,
            'dataProviderSparePart'=>$dataProviderSparePart,
            'searchModel'=>$searchModel,
        ]
    );
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        $this->layout = 'login';

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
