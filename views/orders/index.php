<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OrdersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Orders');
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
$this->registerJs('
    $("#modalButton").click(function(){
         
            $("#modal").modal("show")
                .find("#modalContent")
                .load($(this).attr("value"));
            
       
    });

');

?>
<?php 
    Modal::begin([
        'header'=>'<h3>Received  '.$this->title.'</h3>',
        'id'=>'modal',
        'size'=>'modal-lg',
    ]);
    echo "<div id='modalContent'></div>";
    Modal::end();
    

?>
<div class="orders-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'New Order'), ['create'], ['class' => 'btn btn-primary pull-right']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'order_id',
            'supplier',
            'date',
            'description',
            'status',
            //'created_at',
            //'created_by',
            //'updated_at',
            //'updated_by',
            [
                
               
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, app\models\Orders $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'order_id' => $model->order_id]);
                 }
        
                
            ],

        //    [
        //     'class' => 'yii\grid\ActionColumn',
        //     // Html::button(Yii::t('app', ' Add Spare parts'), ['value'=>Url::to(['/order-details/create','order_id' => $model->order_id]),'class' => 'btn btn-success pull-right','id'=>'modalButton'])
        //     'template' => '{vprint}',
                
        //     'buttons' => [
        //         'vprint' => function ($url, $model, $key) {

        //             return Html::a('<button class="btn btn-success pull-right" id="modalButton"><i>Received</i></button>',['/in-stock/create', 'order_id'=>$model->order_id]
        //             ,
        //         );
                    
                    
        //         },

        //     ],
        //   ],
        ],
    ]); ?>


</div>
