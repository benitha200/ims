<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SpareParts */

$this->title = Yii::t('app', 'Create Spare Parts');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Spare Parts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="spare-parts-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
