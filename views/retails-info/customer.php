<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Retails */

$this->title = Yii::t('app', 'Create Customer');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Retails'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="retails-customer">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_customer_form', [
        'model' => $model,
    ]) ?>

</div>
