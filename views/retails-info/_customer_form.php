<?php

use app\models\Customer;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Retails */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="retails-customer_form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'customer')->dropDownList(
ArrayHelper::map(Customer::find()->all(),'phone','name'),['prompt'=>'']
    ) ?>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'continue'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
