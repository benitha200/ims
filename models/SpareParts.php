<?php

namespace app\models;
use app\traits\AuditTrailsTrait;
use Yii;

/**
 * This is the model class for table "spare_parts".
 *
 * @property string $reference
 * @property string $name
 * @property int $quantity
 * @property int $price
 * @property int $min_stock
 * @property string $unit
 * @property int $created_at
 * @property int $created_by
 * @property int $updated_at
 * @property int $updated_by
 *
 * @property OrderDetails[] $orderDetails
 * @property Orders[] $orders
 * @property Retails[] $retails
 */
class SpareParts extends \yii\db\ActiveRecord
{
    use AuditTrailsTrait;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'spare_parts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['reference', 'name', 'quantity', 'price', 'min_stock', 'unit'], 'required'],
            [['quantity', 'price', 'min_stock', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['reference', 'unit'], 'string', 'max' => 50],
            [['name'], 'string', 'max' => 60],
            [['reference'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'reference' => Yii::t('app', 'Reference'),
            'name' => Yii::t('app', 'Name'),
            'quantity' => Yii::t('app', 'Quantity'),
            'price' => Yii::t('app', 'Price'),
            'min_stock' => Yii::t('app', 'Min Stock'),
            'unit' => Yii::t('app', 'Unit'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * Gets query for [[OrderDetails]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrderDetails()
    {
        return $this->hasMany(OrderDetails::className(), ['spare_part' => 'reference']);
    }

    /**
     * Gets query for [[Orders]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Orders::className(), ['order_id' => 'order_id'])->viaTable('order_details', ['spare_part' => 'reference']);
    }

    /**
     * Gets query for [[Retails]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRetails()
    {
        return $this->hasMany(Retails::className(), ['spare_part' => 'reference']);
    }
}
