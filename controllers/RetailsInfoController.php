<?php

namespace app\controllers;

use app\models\InStock;
use app\models\RetailsInfo;
use app\models\RetailsInfoSearch;
use app\models\SpareParts;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RetailsInfoController implements the CRUD actions for RetailsInfo model.
 */
class RetailsInfoController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all RetailsInfo models.
     *
     * @return string
     */
    public function actionIndex($retails_id)
    {
        $searchModel = new RetailsInfoSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);
        $dataProvider->query->andWhere(['retails_id'=>$retails_id]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RetailsInfo model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RetailsInfo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate($retails_id,$spare_part)
    {
        $model = new RetailsInfo();

        $model->retails_id=$retails_id;
        $model->spare_part=$spare_part;

        $model->quantity=SpareParts::find()->where(['reference'=>$spare_part])->one()->quantity;
        $qua=SpareParts::find()->where(['reference'=>$spare_part])->one()->quantity;
        $model->price=SpareParts::find()->where(['reference'=>$spare_part])->one()->price;
        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {

                if($model->quantity > $qua)
                {
                    \Yii::$app->session->setFlash('error',"the quantity overweights the stock");
                }
                else{
                    if($model->save()){
                    
                    $remaining_quantity=$qua - $model->quantity;

                    \Yii::$app->db->createCommand("UPDATE spare_parts SET quantity='$remaining_quantity' WHERE reference='$spare_part'")->execute();
                    if($remaining_quantity==0){
                        \Yii::$app->db->createCommand("UPDATE spare_parts SET price=0 where reference='$spare_part'")->execute();
                    }
                   return $this->redirect(['/retails/stock', 'retails_id' => $model->retails_id]); 
                }
                }
                
                
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing RetailsInfo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing RetailsInfo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $retails_id=RetailsInfo::find()->where(['id'=>$id])->one()->retails_id;
        $quantity=RetailsInfo::find()->where(['id'=>$id])->one()->quantity;
        $spare_part=RetailsInfo::find()->where(['id'=>$id])->one()->spare_part;

        $stock_quantity=SpareParts::find()->where(['reference'=>$spare_part])->one()->quantity;

        $remaining_quantity=$quantity+$stock_quantity;

        \Yii::$app->db->createCommand("UPDATE spare_parts SET quantity='$remaining_quantity' WHERE reference='$spare_part'")->execute();
        $this->findModel($id)->delete();

        return $this->redirect(['/retails/stock','retails_id'=>$retails_id]);
       // return $this->goBack();
    }

    /**
     * Finds the RetailsInfo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return RetailsInfo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RetailsInfo::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(\Yii::t('app', 'The requested page does not exist.'));
    }
}
