<?php

namespace app\models;
use app\traits\AuditTrailsTrait;
use Yii;

/**
 * This is the model class for table "order_details".
 *
 * @property int $order_id
 * @property string $spare_part
 * @property int $quantity
 * @property int $price
 * @property int $created_at
 * @property int $created_by
 * @property int $updated_at
 * @property int $updated_by
 *
 * @property InStock $inStock
 * @property Orders $order
 * @property SpareParts $sparePart
 */
class OrderDetails extends \yii\db\ActiveRecord
{
    use AuditTrailsTrait;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['spare_part', 'quantity', 'price'], 'required'],
            [['order_id', 'quantity', 'price', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['spare_part'], 'string', 'max' => 50],
            [['order_id', 'spare_part'], 'unique', 'targetAttribute' => ['order_id', 'spare_part']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['order_id' => 'order_id']],
            [['spare_part'], 'exist', 'skipOnError' => true, 'targetClass' => SpareParts::className(), 'targetAttribute' => ['spare_part' => 'reference']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'order_id' => Yii::t('app', 'Order ID'),
            'spare_part' => Yii::t('app', 'Spare Part'),
            'quantity' => Yii::t('app', 'Quantity'),
            'price' => Yii::t('app', 'Price'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * Gets query for [[InStock]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInStock()
    {
        return $this->hasOne(InStock::className(), ['order_id' => 'order_id', 'spare_part' => 'spare_part']);
    }

    /**
     * Gets query for [[Order]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Orders::className(), ['order_id' => 'order_id']);
    }

    /**
     * Gets query for [[SparePart]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSparePart()
    {
        return $this->hasOne(SpareParts::className(), ['reference' => 'spare_part']);
    }
}
