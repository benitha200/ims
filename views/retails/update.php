<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Retails */

$this->title = Yii::t('app', 'Update Retails: {name}', [
    'name' => $model->retails_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Retails'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->retails_id, 'url' => ['view', 'retails_id' => $model->retails_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="retails-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
