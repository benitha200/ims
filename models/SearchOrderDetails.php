<?php

namespace app\models;

use app\models\OrderDetails;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Orders;
use app\models\SpareParts;

/**
 * SearchPartOut represents the model behind the search form of `app\modules\api\modules\v1\models\PartOut`.
 */
class SearchOrderDetails extends OrderDetails
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['spare_part', 'quantity', 'price'], 'required'],
            [['order_id', 'quantity', 'price', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['spare_part'], 'string', 'max' => 50],
            [['order_id', 'spare_part'], 'unique', 'targetAttribute' => ['order_id', 'spare_part']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['order_id' => 'order_id']],
            [['spare_part'], 'exist', 'skipOnError' => true, 'targetClass' => SpareParts::className(), 'targetAttribute' => ['spare_part' => 'reference']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$order_id=null)
    {

        $query = OrderDetails::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if(!is_null($order_id)){
        $query->AndWhere([
            'order_id'=>$order_id]);
    }
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'order_id' => $this->order_id,
            'part' => $this->spare_part,
            'quantity' => $this->quantity,
            'price' => $this->purchase,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'order_id', $this->order_id]);

        return $dataProvider;
    }
}
