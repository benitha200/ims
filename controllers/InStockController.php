<?php

namespace app\controllers;

use app\models\InStock;
use app\models\InStockSearch;
use app\models\OrderDetails;
use app\models\SpareParts;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * InStockController implements the CRUD actions for InStock model.
 */
class InStockController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all InStock models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new InStockSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single InStock model.
     * @param int $order_id Order ID
     * @param string $spare_part Spare Part
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($order_id, $spare_part)
    {
        return $this->render('view', [
            'model' => $this->findModel($order_id, $spare_part),
        ]);
    }

    /**
     * Creates a new InStock model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate($order_id,$spare_part)
    {
        
        $model = new InStock();
        $model->order_id=$order_id;
        //$spare_part=OrderDetails::find()->where(['order_id'=>$order_id])->one();
        $model->spare_part=$spare_part;
        
        $quantity_price=OrderDetails::find()->where(['order_id'=>$order_id,'spare_part'=>$spare_part])->one();
        $model->quantity=$quantity_price->quantity;
        $model->price=$quantity_price->price;
        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                //return $this->redirect(['/orders/index']);
                //$spare=InStock::find()->where(['spare_part'=>$model->spare_part])->one();
         
                    if($model->save()){

                    $stock_quantity=SpareParts::find()->where(['reference'=>$model->spare_part])->one()->quantity;
                    $stock_price=SpareParts::find()->where(['reference'=>$model->spare_part])->one()->price;
                    $total_quantity=$stock_quantity+$model->quantity;

                    $quantity=$model->quantity;
                    $pricee=$model->price;
                    if($stock_price != 0 && $stock_quantity != 0){
                        $price=((($quantity * $pricee )+($stock_quantity * $stock_price)) / ($stock_quantity+$quantity));
                        \Yii::$app->db->createCommand("UPDATE spare_parts Set quantity='$total_quantity', price='$price' WHERE reference='$model->spare_part'")->execute();


                    }
                    else{
                        $price=$model->price;
                        \Yii::$app->db->createCommand("UPDATE spare_parts Set quantity='$total_quantity', price='$price' WHERE reference='$model->spare_part'")->execute();

                    }
                    $model->updateStatus($model->order_id);
                    
                    return $this->redirect(['view', 'order_id' => $model->order_id, 'spare_part' => $model->spare_part]);
                    
                }
             
                
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing InStock model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $order_id Order ID
     * @param string $spare_part Spare Part
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($order_id, $spare_part)
    {
        $model = $this->findModel($order_id, $spare_part);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'order_id' => $model->order_id, 'spare_part' => $model->spare_part]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing InStock model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $order_id Order ID
     * @param string $spare_part Spare Part
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($order_id, $spare_part)
    {
        $this->findModel($order_id, $spare_part)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the InStock model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $order_id Order ID
     * @param string $spare_part Spare Part
     * @return InStock the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($order_id, $spare_part)
    {
        if (($model = InStock::findOne(['order_id' => $order_id, 'spare_part' => $spare_part])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(\Yii::t('app', 'The requested page does not exist.'));
    }
}
