<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Customer;
use kartik\select2\Select2;


/* @var $this yii\web\View */
/* @var $model app\models\Retails */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="retails-form">

    <?php $form = ActiveForm::begin(); ?>

    <!-- <?= $form->field($model, 'customer')->textInput() ?> -->

<?= $form->field($model, 'customer')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Customer::find()->all(), 'phone', function ($model) {
                        return $model->name;
                    }),
                    'options' => ['placeholder' => Yii::t('app', 'Select Customer')],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ])  ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

   
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
