﻿
<!DOCTYPE html>
<html lang="en">
<head>
<script type="text/javascript">
//  window.onload = function() { window.print(); }
window.onload = function() { window.print();
    
 // window.onafterprint(history.back());
  // window.afterclick(history.back());
}

</script>

    <meta charset="UTF-8">
    <title>DotNetTec - Invoice html template bootstrap</title>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta.2/css/bootstrap.css'>
</head>

<body>


<?php
$conn=new mysqli("localhost","root","","stock");
if($conn->connect_error){
    die("connection failed : ".$conn->connect_error);
}

$id=$_REQUEST['retails_id'];
$sql="select spare_part,quantity,price,discount,(quantity*price) as total from retails_info where retails_id=$id";

$statment="select sum((quantity*price)-((quantity*price*discount)/100)) as TOT from retails_info where retails_id=$id";

$tot=$conn->query($statment);

$cust="select c.phone, c.name, c.address from customer c, retails r where r.customer=c.phone and retails_id=3";
$customer=$conn->query($cust);

$result=$conn->query($sql);
?>

    <div class="container">
        <div class="card">
            <div class="card-h" style="text-align: center;" >
                <strong>[COMPANY NAME]</strong><br> 
                <strong>[COMPANY ADDRESS]</strong><br>
                <strong>[PHONE NUMBER]</strong><br>
                <strong>[FAX NUMBER]</strong> <br>
                <strong>[EMAIL ADDRESS]</strong><br>
                <strong>PRICE QUOTATION</strong>

            </div><br><br>
            <div class="card-header" style="background-color: rgb(80, 97, 150);">
                <strong>Customer Information</strong>
                <!-- <span class="float-right"> <strong>Status:</strong> Pending</span> -->
               
            </div>
            <div class="card-body">
                <div class="row mb-4">
                    <div class="col-sm-6">
                        <!-- <div>
                            <strong>DotNetTec</strong>
                        </div> -->
                        <?php   $ro = mysqli_fetch_array($customer); ?>

                        <div><strong>Name: </strong><?php echo $ro['name'];?></div>
                        <div><strong>Address: </strong><?php echo $ro['address'];?></div>
                        <div><strong>Zip/Postal Code: </strong></div>
                        <div><strong>Phone Number: </strong> <?php echo "0".$ro['phone']; ?></div>
                    </div>
                    
                    <div class="col-sm-6">
                        <!-- <h6 class="mb-3">To:</h6>
                        <div>
                            <strong>Robert Maxwel</strong>
                        </div> -->
                        <div><strong>Date: </strong><?php echo date('Y-m-d') ?></div>
                        <div><strong>Quote No: </strong></div>

                    </div>
                </div>

                <div class="table-responsive-sm">
                    <table class="table table-bordered">
                        <thead>
                            <tr style="background-color: rgb(80, 97, 150);">
                                <th class="center">Item Name</th>
                                <th class="right">Unit Cost</th>
                                <th class="center">Qty</th>
                                <th class="center">Discount</th>
                                <th class="right">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php  
                          
                          if($result->num_rows > 0){
                                while($row = $result->fetch_array()){
                                  echo "<tr><td>".$row["spare_part"]."</td><td>".$row["price"]."</td><td>".$row["quantity"]."</td><td>".$row["discount"]."</td><td>".$row["total"]."</td></tr>";
                                }
                            }      
                             
                            ?>
       
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-sm-5">

                    </div>

                    <div class="col-lg-4 col-sm-5 ml-auto">
                        <table class="table table-clear">
                            <tbody>
                                <tr>
                                    <td class="left">
                                        <strong>Subtotal</strong>
                                    </td>
                                    <td class="right">
                                        
                                   <?php
                                   while ($r = mysqli_fetch_array($tot)) {
                                    echo $r['TOT'];
                                
                                    //echo $row["TOT"];
                                ?></td>
                                </tr>
                                <tr>
                                    <td class="left">
                                        <strong>Discount (20%)</strong>
                                    </td>
                                    <td class="right"> <?php
                                    //  echo $row["discount"];
                              ?></td>
                                </tr>

                                <tr>
                                    <td class="left">
                                        <strong>Total amount</strong>
                                    </td>
                                    <td class="right">
                                        <strong> <?php
                                         
                                            echo $r['TOT'];
                                        }
                                    //echo $row["TOT"];
                                 ?></strong>
                                    </td>
                                </tr>

                           </tbody>

                        </table>
                     <?php 
                    $conn->close();
                     ?>
                    </div>
                    
                    <p><strong>Note:</strong> the customer shall be billed out upon acceptance of this price quotation.Payment due to the prior to the delivery of items</p><br><br><br>
                   <footer style="font-size: smaller;">For further inquiries please contact [COMPANY CONTACT NUMBER] or email us at [E-MAIL ADDRESS]</footer>
                </div>
            </div>
          
        </div>
    </div>
</body>
</html>
