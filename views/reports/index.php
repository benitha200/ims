<?php
use yii\helpers\Html;

$this->title='Find report'
?>
<html>
    <head>

    </head>
    <body>
        <div class="panel panel-default">
            <div class="panel-heading">
                <?php
                Yii::t('app','Select the report you want and range of dates then click find to see the results');
                ?>
            </div>
            <div class="panel-body">
                <?= $this->render('_reportsForm',['model'=>$model])?>
                <p>
        <?= Html::a(Yii::t('app','Monthly Stock Report'), ['monthly-report'], ['class' => 'btn btn-primary pull-right']) ?>
         </p>
            </div>
            <div class="panel-footer">
                <?php echo date('Y-m-d') ?>
            </div>
        </div>


    </body>
</html>
