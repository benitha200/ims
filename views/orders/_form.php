<?php

use app\models\Supplier;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap4\ActiveForm;
use kartik\select2\Select2;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Orders */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orders-form">

    <?php $form = ActiveForm::begin(); ?>


    <!-- <?= $form->field($model, 'supplier')->textInput(['maxlength' => true]) ?> -->

    
    <?= $form->field($model, 'supplier')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Supplier::find()->all(), 'name', function ($model) {
                        return $model->name;
                    }),
                    'options' => ['placeholder' => Yii::t('app', 'Select supplier')],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ])  ?>

  
<?= $form->field($model, 'date')->widget(DatePicker::classname(), [
    'name' => 'check_date',
    //'value' => '01/29/2014',
    'removeButton' => false,
    'pluginOptions' => [
        'autoclose' => true,
        'format' => 'yyyy-mm-dd'
    ]
]); ?>


    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
