<?php
namespace app\models;

use yii\base\Model;
use Yii;

class DocumentReport extends Model
{
    public $DocumentReport;

    public function rules()
    {
        [['DocumentReport'],'required'];
        [['DocumentReport'],'string'];
    }

    public function attributeLabels()
    {
        return[
           'documentReport'=>Yii::t('app','Document Report'),
        ];
        
    }

}

?>