<?php

namespace app\models;
use app\traits\AuditTrailsTrait;
use Yii;

/**
 * This is the model class for table "retails_info".
 *
 * @property int $id
 * @property int $quantity
 * @property int $price
 * @property int $discount
 * @property int $created_at
 * @property int $created_by
 * @property int $updated_at
 * @property int $updated_by
 * @property string $spare_part
 * @property int $retails_id
 *
 * @property Retails $retails
 * @property SpareParts $sparePart
 */
class RetailsInfo extends \yii\db\ActiveRecord
{
    use AuditTrailsTrait;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'retails_info';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['quantity', 'price', 'discount','spare_part', 'retails_id'], 'required'],
            [['quantity', 'price', 'discount', 'created_at', 'created_by', 'updated_at', 'updated_by', 'retails_id'], 'integer'],
            [['spare_part'], 'string', 'max' => 50],
            [['spare_part'], 'exist', 'skipOnError' => true, 'targetClass' => SpareParts::className(), 'targetAttribute' => ['spare_part' => 'reference']],
            [['retails_id'], 'exist', 'skipOnError' => true, 'targetClass' => Retails::className(), 'targetAttribute' => ['retails_id' => 'retails_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'quantity' => Yii::t('app', 'Quantity'),
            'price' => Yii::t('app', 'Price'),
            'discount' => Yii::t('app', 'Discount'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'spare_part' => Yii::t('app', 'Spare Part'),
            'retails_id' => Yii::t('app', 'Retails ID'),
        ];
    }

    /**
     * Gets query for [[Retails]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRetails()
    {
        return $this->hasOne(Retails::className(), ['retails_id' => 'retails_id']);
    }

    /**
     * Gets query for [[SparePart]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSparePart()
    {
        return $this->hasOne(SpareParts::className(), ['reference' => 'spare_part']);
    }
}
