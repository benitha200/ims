<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use app\models\RetailsInfo;
use app\models\SpareParts;
use kartik\form\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
use app\models\Month;
// $model = new \yii\base\DynamicModel(['documentReports']);

$this->title=Yii::t('app','Monthly Report');

$month=[['id'=>'01','name'=>'january'],['id'=>'02','name'=>'february'],['id'=>'03','name'=>'march'],['id'=>'04','name'=>'April'],['id'=>'05','name'=>'May'],['id'=>'06','name'=>'June'],['id'=>'07','name'=>'July'],['id'=>'08','name'=>'August'],['id'=>'09','name'=>'September'],['id'=>'10','name'=>'October'],['id'=>'11','name'=>'November'],['id'=>'12','name'=>'December']];
?>

<?php $form = ActiveForm::begin(); ?>
<div class="row">
  <div class="col-md-6">
<?php echo
 $form->field($model, 'month')->widget(Select2::classname(), [
        'data' => ArrayHelper::map($month,'id','name'),
        'options' => ['placeholder' => Yii::t('app', 'Select Month')],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ]) 
     ?>
    </div>
  <div class="col-md-6">

<?php echo
 $form->field($model, 'year')->widget(Select2::classname(), [
        'data' => $model->getYearList(),
        'options' => ['placeholder' => Yii::t('app', 'Select Year')],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ]) 

//$form->dropDownList($model,'year',$model->getYearList());
     ?></div></div>
<div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>

            <!-- <?= Html::submitButton('Search', ['name' => 'action', 'value' => 'submit_1']) ?> -->
        </div> 
<?php ActiveForm::end(); ?>

<?=GridView::widget([
  'dataProvider'=>$dataProvider,
  'summary'=>Yii::t('app','Showing {totalCount}'),
  'showPageSummary'=>true,
  'panel'=>['type'=>'info','heading'=>$this->title],
  'columns'=>[
     ['class'=>'kartik\grid\SerialColumn'],
     //'spare_part',

    //  [
    //    'label'=>'Customer',
    //    'attribute'=>'name',
    //    'format'=>'html',
    //    'value'=>function($model) 
    //    {
    //        return Html::a($model['name'], ['customer-details', 'retails_id'=>$model['retails_id'],'start'=>$_REQUEST['start'],'end'=>$_REQUEST['end']]);
    //    }
    //  ],
    [
        'label'=>'Spare Part',
        'attribute'=>'reference',
         //'pageSummary'=>true,
      ],
     [
       'attribute'=>'quantity',
        'pageSummary'=>true,
     ],
     [
        'attribute'=>'price',
        'pageSummary'=>true,
      ],


    ],
  // 'rowOptions'   => function ($model, $key, $index, $grid) {
  //   return ['spare_part' => $model->spare_part];
//},


]);
?>