<?php

use kartik\grid\GridView;
use yii\helpers\Html;


$this->title = Yii::t('app', 'Retails');
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- 
<div class="retails-receipt">

    <h1><?= Html::encode($this->title) ?></h1>

 <?= GridView::widget([
        'dataProvider' => $RetaildataProvider,
        //'showFooter' => true, // show footer section of the gridview widget
        //'footerRowOptions'=>['style'=>'font-weight:bold;text-decoration: underline;'],
        'showPageSummary' => true,
        //'filterModel' => $searchModel2,
       // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label'=>'Spare part',
                'attribute'=>'spare_part',
                'pageSummary'=>'ToTal',
            ],
            //'customer',
            'quantity',
            'price',
            'discount',

            [
                'label' =>'Amount',
                'class' => '\kartik\grid\FormulaColumn',
                'value' => function ($model, $key, $index, $widget) {
                    $p = compact('model', 'key', 'index');
                    // Write your formula below
                    if($widget->col(4, $p) != 0){
                       $dis= ($widget->col(2, $p) * $widget->col(3, $p) * $widget->col(4, $p))/100;
                       return  ($widget->col(2, $p) * $widget->col(3, $p)) - $dis;
                    }
                    else{
                        return $widget->col(2, $p) * $widget->col(3, $p);
                    }
                    
                },
                'pageSummary'=>true,
            ],
           

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{vprint}',
                'buttons' => [
                    'vprint' => function ($url, $model, $key) {
                        return Html::a('<i class="glyphicon glyphicon-trash" style="font-size:px;color:red"></i>',['retails-info/delete','id'=>$model->id]
                        ,
                        [
                            //'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                            'method' => 'post',
                            ],
                        ]);
                    },

                ],
            
            ],
            
        ],
       
    ]);
    ?>
</div> -->


<?php
$this->widget('bootstrap.widgets.TbExtendedGridView', array(
	'fixedHeader' => true,
	'headerOffset' => 40,
	'type' => 'striped',
	'dataProvider' => $dataReportItem,
	'responsiveTable' => true,
	'template' => "{items}",
        'columns'=>array(
            'spare_part',
            'quantity',
            'price',
            // 'ITEM_DESC',            
            // 'CREATED_DATE', // This is a reference for parms / parameters
        ),
	));?>
<div align="left">
    <b>Printed By : <? echo Yii::$app()->user->name;?><br/>
Printed At : <? echo date("d/m/Y H:i:s");?></b>
            <div align="right">Copyright &COPY; <?php echo date('Y'); ?> By Jsource</div>
</div>
