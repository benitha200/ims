<?php

namespace app\controllers;

use app\models\Retails;
use app\models\RetailsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\InStockSearch;
use app\models\SparePartsSearch;;
use app\models\RetailsInfoSearch;
use yii\data\SqlDataProvider;
use app\models\CustomerRetailsSearch;
//use app\models\Pdfprint;

/**
 * RetailsController implements the CRUD actions for Retails model.
 */
class RetailsController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Retails models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new RetailsSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Retails model.
     * @param int $retails_id Retails ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($retails_id)
    {
        \yii\helpers\Url::remember();

        //$searchModel2= new CustomerRetailsSearch();
        // $RetaildataProvider= $searchModel2->search($this->request->queryParams);
        // $RetaildataProvider->query->andWhere(['retails_id'=>$retails_id]);

        $searchModel = new RetailsInfoSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);
        $dataProvider->query->andWhere(['retails_id'=>$retails_id]);

        //$this->renderPartial('/retails-info/index');
        return $this->render('view', [
            'model' => $this->findModel($retails_id),
            'dataProvider'=>$dataProvider,
            'searchModel'=>$searchModel,
        ]);
    }

    /**
     * Creates a new Retails model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Retails();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['stock', 'retails_id' => $model->retails_id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionStock($retails_id)
    {

        \yii\helpers\Url::remember();

        $searchModel = new SparePartsSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        $model=new RetailsInfoSearch();
        $model->retails_id=$retails_id;

        $customer=Retails::find()->where(['retails_id'=>$retails_id])->one()->customer;
        

        //$searchCustomer = new Customer();
        $sql="SELECT * FROM customer WHERE phone=$customer";
        $count = \Yii::$app->db->createCommand("SELECT COUNT(*) FROM ($sql) t")->queryScalar();

        $CustomerdataProvider = new SqlDataProvider([
            'sql' => $sql,
            'totalCount' => $count,
            //'params' => $params,
            'pagination' => [
                'pageSize' => false,
            ],
        ]);

        $searchModel2= new CustomerRetailsSearch();
        $RetaildataProvider= $searchModel2->search($this->request->queryParams);
        $RetaildataProvider->query->andWhere(['retails_id'=>$retails_id]);
        return $this->render('stock', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'CustomerdataProvider' => $CustomerdataProvider,
            'RetaildataProvider'=>$RetaildataProvider,
            //'searchModel2'=>$searchModel2,

        ]);
    }


    public function actionInvoice($retails_id)
    {

        $this->layout = 'login';
        \yii\helpers\Url::remember();

        // $searchModel = new InStockSearch();
        // $dataProvider = $searchModel->search($this->request->queryParams);

        // $model=new RetailsInfoSearch();
        // $model->retails_id=$retails_id;


        // $searchModel2= new CustomerRetailsSearch();
        // $RetaildataProvider= $searchModel2->search($this->request->queryParams);
        // $RetaildataProvider->query->andWhere(['retails_id'=>$retails_id]);
        return $this->render('invoice1', [
            // 'searchModel' => $searchModel,
            // 'dataProvider' => $dataProvider,
            // 'CustomerdataProvider' => $CustomerdataProvider,
           // 'RetaildataProvider'=>$RetaildataProvider,
            //'searchModel2'=>$searchModel2,

        ]);
    }
    public function actionReceipt($retails_id)
    {

        \yii\helpers\Url::remember();

        // $searchModel = new InStockSearch();
        // $dataProvider = $searchModel->search($this->request->queryParams);

        $model=new RetailsInfoSearch();
        $model->retails_id=$retails_id;

        $customer=Retails::find()->where(['retails_id'=>$retails_id])->one()->customer;
        

        //$searchCustomer = new Customer();
        // $sql="SELECT * FROM customer WHERE phone=$customer";
        // $count = \Yii::$app->db->createCommand("SELECT COUNT(*) FROM ($sql) t")->queryScalar();

        // $CustomerdataProvider = new SqlDataProvider([
        //     'sql' => $sql,
        //     'totalCount' => $count,
        //     //'params' => $params,
        //     'pagination' => [
        //         'pageSize' => false,
        //     ],
        // ]);

        // $rsql="SELECT * FROM retails_info WHERE retails_id=$retails_id";
        // $rcount = \Yii::$app->db->createCommand("SELECT COUNT(*) FROM ($rsql) t")->queryScalar();

        // $RetaildataProvider = new SqlDataProvider([
        //     'sql' => $rsql,
        //     'totalCount' => $rcount,
        //     //'params' => $params,
        //     'pagination' => [
        //         'pageSize' => false,
        //     ],
        // ]);

        $searchModel2= new CustomerRetailsSearch();
        $RetaildataProvider= $searchModel2->search($this->request->queryParams);
        $RetaildataProvider->query->andWhere(['retails_id'=>$retails_id]);
        return $this->render('receipt', [
            // 'searchModel' => $searchModel,
            // 'dataProvider' => $dataProvider,
            // 'CustomerdataProvider' => $CustomerdataProvider,
            'RetaildataProvider'=>$RetaildataProvider,
            //'searchModel2'=>$searchModel2,

        ]);
    }

    /**
     * Updates an existing Retails model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $retails_id Retails ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($retails_id)
    {
        $model = $this->findModel($retails_id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['stock', 'retails_id' => $model->retails_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Retails model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $retails_id Retails ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($retails_id)
    {
        $this->findModel($retails_id)->delete();

        return $this->redirect(['index']);
        
    }

    /**
     * Finds the Retails model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $retails_id Retails ID
     * @return Retails the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($retails_id)
    {
        if (($model = Retails::findOne(['retails_id' => $retails_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(\Yii::t('app', 'The requested page does not exist.'));
    }
}
