<?php

namespace app\models;
use app\traits\AuditTrailsTrait;
use Yii;

/**
 * This is the model class for table "in_stock".
 *
 * @property int $order_id
 * @property string $spare_part
 * @property int $quantity
 * @property int $price
 * @property int $created_at
 * @property int $created_by
 * @property int $updated_at
 * @property int $updated_by
 *
 * @property OrderDetails $order
 */
class InStock extends \yii\db\ActiveRecord
{
    use AuditTrailsTrait;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'in_stock';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'spare_part', 'quantity', 'price'], 'required'],
            [['order_id', 'quantity', 'price', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['spare_part'], 'string', 'max' => 50],
            [['order_id', 'spare_part'], 'unique', 'targetAttribute' => ['order_id', 'spare_part']],
            [['order_id', 'spare_part'], 'exist', 'skipOnError' => true, 'targetClass' => OrderDetails::className(), 'targetAttribute' => ['order_id' => 'order_id', 'spare_part' => 'spare_part']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'order_id' => Yii::t('app', 'Order ID'),
            'spare_part' => Yii::t('app', 'Spare Part'),
            'quantity' => Yii::t('app', 'Quantity'),
            'price' => Yii::t('app', 'Price'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * Gets query for [[Order]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(OrderDetails::className(), ['order_id' => 'order_id', 'spare_part' => 'spare_part']);
    }

    public function updateStatus($order)
    {
        $items=OrderDetails::find()->where(['order_id'=>$order])->all();
        foreach($items as $item)
        {
            $in=InStock::find()->where(['order_id'=>$item->order_id,'spare_part'=>$item->spare_part])->one();
            if(empty($in))
            {
                return false;
            }
        }
        $order=Orders::find()->where(['order_id'=>$order])->one();
        $order->status=1;
        $order->save();
        return true;
    }

}
