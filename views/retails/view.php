<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use kartik\grid\GridView;
/* @var $this yii\web\View */
/* @var $model app\models\Retails */

$this->title = $model->retails_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Retails'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="retails-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'retails_id' => $model->retails_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'retails_id' => $model->retails_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Print', ['/retails/invoice','retails_id'=>$_REQUEST['retails_id']], ['class'=>'btn btn-success']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'retails_id',
            'description',
            'created_at',
            'created_by',
            'updated_at',
            'updated_by',
            'customer',
        ],
    ]) ?>

<?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'showFooter' => true, // show footer section of the gridview widget
        //'footerRowOptions'=>['style'=>'font-weight:bold;text-decoration: underline;'],
        'showPageSummary' => true,
        //'filterModel' => $searchModel2,
       // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],
            //'id',
            [
                'label'=>'Spare part',
                'attribute'=>'spare_part',
                'pageSummary'=>'ToTal',
            ],
            //'customer',
            'quantity',
            'price',
            'discount',

            [
                'label' =>'Amount',
                'class' => '\kartik\grid\FormulaColumn',
                'value' => function ($model, $key, $index, $widget) {
                    $p = compact('model', 'key', 'index');
                    // Write your formula below
                    if($widget->col(4, $p) != 0){
                       $dis= ($widget->col(2, $p) * $widget->col(3, $p) * $widget->col(4, $p))/100;
                       return  ($widget->col(2, $p) * $widget->col(3, $p)) - $dis;
                    }
                    else{
                        return $widget->col(2, $p) * $widget->col(3, $p);
                    }
                    
                },
                'pageSummary'=>true,
            ],
           


            // [
            //     'class' => 'yii\grid\ActionColumn',
            //     'template' => '{vprint}',
            //     'buttons' => [
            //         'vprint' => function ($url, $model, $key) {
            //             return Html::a('<i class="glyphicon glyphicon-trash" style="font-size:px;color:red"></i>',['retails-info/delete','id'=>$model->id]
            //             ,
            //             [
            //                 //'class' => 'btn btn-danger',
            //                 'data' => [
            //                     'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
            //                 'method' => 'post',
            //                 ],
            //             ]);
            //         },

            //     ],
            
            // ],
    ], 
    ]);
?>
   
    
    
    

</div>
