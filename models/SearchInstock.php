<?php

namespace app\models;

use app\models\OrderDetails;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Orders;
use app\models\SpareParts;

/**
 * SearchPartOut represents the model behind the search form of `app\modules\api\modules\v1\models\PartOut`.
 */
class SearchInstock extends InStock
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'spare_part', 'quantity', 'price'], 'required'],
            [['order_id', 'quantity', 'price', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['spare_part'], 'string', 'max' => 50],
            [['order_id', 'spare_part'], 'unique', 'targetAttribute' => ['order_id', 'spare_part']],
            [['order_id', 'spare_part'], 'exist', 'skipOnError' => true, 'targetClass' => OrderDetails::className(), 'targetAttribute' => ['order_id' => 'order_id', 'spare_part' => 'spare_part']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search()
    {

        $query = InStock::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
    
        // // grid filtering conditions
        // $query->andFilterWhere([
        //     'order_id' => $this->order_id,
        //     'part' => $this->spare_part,
        //     'quantity' => $this->quantity,
        //     'price' => $this->purchase,
        //     'created_at' => $this->created_at,
        //     'updated_at' => $this->updated_at,
        //     'created_by' => $this->created_by,
        //     'updated_by' => $this->updated_by,
        // ]);

        // $query->andFilterWhere(['like', 'order_id', $this->order_id]);

        return $dataProvider;
    }
}
