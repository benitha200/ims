<?php

namespace app\models;
use app\traits\AuditTrailsTrait;
use Yii;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $name
 * @property string $username
 * @property string $phone_number
 * @property string $password
 * @property string|null $authKey
 * @property int created_at
 * @property int created_by
 * @property int updated_at
 * @property int updated_by
 */
class Users extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    use AuditTrailsTrait;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'username','phone', 'password'], 'required'],
            [['phone', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['username'], 'string', 'max' => 100],
            [['password', 'authKey'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'username' => 'Username',
            'phone' => 'Phone Number',
            'password' => 'Password',
            'authKey' => 'Auth Key',
            'created_at' => 'Created At',
            'created_by' =>'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
    // public function beforeSave($insert)
    // {
    //     if ($this->scenario=='register') {
    //         $this->generateHash();
    //     } elseif (!empty($this->password)) {
    //         $this->generateHash();
    //     }

    //     return parent::beforeSave($insert);
    // }
    // public function generateHash()
    // {
    //     $this->password_hash = Yii::$app->getSecurity()->generatePasswordHash($this->password);
    // }
    public function getAuthKey(){
        return $this->authKey;
    }
    public function getId(){
        return $this->id;
    }
    public function validateAuthKey($authKey){
        return $this->authKey===$authKey;
    }
    public static function findIdentity($id){
        return self::findOne($id);
    }
    public static function findIdentityByAccessToken($token,$type=null){
        throw new \yii2\base\NotSupportedException;
    }
    public static function findByUsername($username){
        return self::findOne(['username'=>$username]);
    }
    public function validatePassword($password){
       return $this->password ===$password;

       // return Yii::$app->getSecurity()->generatePasswordHash( $password);
    }
  
}
