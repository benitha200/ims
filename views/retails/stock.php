<?php

use app\models\Customer;
use app\models\Retails;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use kartik\grid\GridView;

//use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RetailsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Retails');
$this->params['breadcrumbs'][] = $this->title;


?>

<?= \dixonstarter\pdfprint\Pdfprint::widget();?>
<?= \dixonstarter\pdfprint\Pdfprint::widget([
  'elementClass' => '.btn-pdfprint'
]);?>

<a href="receipt.pdf" class="btn-pdfprint">open</a>

<div class="retails-stock">

    <h1><?= Html::encode($this->title) ?></h1>

    <!-- <p>
        <?= Html::a(Yii::t('app', 'Create Retails'), ['customer'], ['class' => 'btn btn-success']) ?>
    </p> -->

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $CustomerdataProvider,
       
       // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'phone',
            //'customer',
            'name',
            'address',
            
        ],
    ]); ?>
<script type="text/javascript">

    function printlayer(Layer)
    {
        var generator=window.open("invoice.html");
        var layetext=document.getElementById(layer);
        generator.document.write(layetext.innerHTML.replace("Print Me"));
        generator.document.close();
        generator.print();
        generator.close();
    }
</script>
<div id="sales">
<?= GridView::widget([
        'dataProvider' => $RetaildataProvider,
        //'showFooter' => true, // show footer section of the gridview widget
        //'footerRowOptions'=>['style'=>'font-weight:bold;text-decoration: underline;'],
        'showPageSummary' => true,
        //'filterModel' => $searchModel2,
       // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label'=>'Spare part',
                'attribute'=>'spare_part',
                'pageSummary'=>'ToTal',
            ],
            //'customer',
            'quantity',
            'price',
            'discount',

            [
                'label' =>'Amount',
                'class' => '\kartik\grid\FormulaColumn',
                'value' => function ($model, $key, $index, $widget) {
                    $p = compact('model', 'key', 'index');
                    // Write your formula below
                    if($widget->col(4, $p) != 0){
                       $dis= ($widget->col(2, $p) * $widget->col(3, $p) * $widget->col(4, $p))/100;
                       return  ($widget->col(2, $p) * $widget->col(3, $p)) - $dis;
                    }
                    else{
                        return $widget->col(2, $p) * $widget->col(3, $p);
                    }
                    
                },
                'pageSummary'=>true,
            ],
            // [
            //     'class' => ActionColumn::className(),
            //     'template' => '{delete}',
            //     'urlCreator' => function ($action, app\models\RetailsInfo $model, $key, $index, $column) {
            //         return Url::toRoute([$action, 'id' => $model->id]);
            //      }
            // ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{vprint}',
                'buttons' => [
                    'vprint' => function ($url, $model, $key) {
                        return Html::a('<i class="glyphicon glyphicon-trash" style="font-size:px;color:red"></i>',['retails-info/delete','id'=>$model->id]
                        ,
                        [
                            //'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                            'method' => 'post',
                            ],
                        ]);
                    },

                ],
            
            ],
            
        ],
       
    ]);
    ?></div>
<!-- <a href="url/test.pdf" class="btn-pdfprint">open</a>
<input type="button"
        
         value="Go Somewhere Else"
         onclick="location.href='<%: Url.Action("Action", "Controller") %>'" /> -->

  <?= Html::a('Print', ['/retails/invoice','retails_id'=>$_REQUEST['retails_id']], ['class'=>'btn btn-primary']) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'label'=>'Spare Part',
                'attribute'=>'reference',
            ],
            //'customer',
            'quantity',
            'price',
            //'discount',
            //'created_at',
            //'created_by',
            //'updated_at',
            //'updated_by',
            [
                'class' => 'yii\grid\ActionColumn',
                // Html::button(Yii::t('app', ' Add Spare parts'), ['value'=>Url::to(['/order-details/create','order_id' => $model->order_id]),'class' => 'btn btn-success pull-right','id'=>'modalButton'])
                'template' => '{vprint}',
                    
                'buttons' => [
                    

                     'vprint' => function ($url, app\models\SpareParts $model, $key) {

    
                        return Html::a('<button class="btn btn-success pull-right" id="modalButton"><i>Sell</i></button>',['/retails-info/create','spare_part'=>$model->reference,'retails_id'=>$_REQUEST['retails_id']]
                        ,
                    );
                        
                        
                    },
    
                ],
            ],
        ],
    ]); ?>

<?= Html::a(Yii::t('app', 'Retails'), ['index'], ['class' => 'btn btn-success']) ?>


</div>
