<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use app\models\Users;
use app\models\InStock;



/* @var $this yii\web\View */
/* @var $model app\models\Orders */

$this->title = $model->order_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

$this->registerJs('
    $("#modalButton").click(function(){
         
            $("#modal").modal("show")
                .find("#modalContent")
                .load($(this).attr("value"));
            
       
    });

');
?>
<?php 
    Modal::begin([
        'header'=>'<h3>New Spare for '.$this->title.'</h3>',
        'id'=>'modal',
        'size'=>'modal-lg',
    ]);
    echo "<div id='modalContent'></div>";
    Modal::end();
    

?>
<div class="orders-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    
       
    <!-- <?= Html::a('label', ['/controller/action'], ['class'=>'btn btn-primary pull-right']) ?> -->
       
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'order_id' => $model->order_id], [
            'class' => 'btn btn-danger pull-right',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?> 
         <?= Html::a(Yii::t('app', 'Update'), ['update', 'order_id' => $model->order_id], ['class' => 'btn btn-primary pull-right']) ?>
        <?= Html::button(Yii::t('app', ' Add Spare parts'), ['value'=>Url::to(['/order-details/create','order_id' => $model->order_id]),'class' => 'btn btn-success pull-right','id'=>'modalButton']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'order_id',
            'supplier',
            'date',
            'description',
          
            [
                'attribute'=>'status',
                'value'=>function($model)
                {
                    if($model->status==1)
                        return 'Paid';
                    return 'Pending';
                }
            ],
            'created_at:datetime',
            [
                'attribute'=>'created_by',
                'value'=>function($model)
                {
                    return Users::find()->where(['id'=>$model->created_by])->one()->name;
                }
            ],
            'updated_at:datetime',
           
            [
                'attribute'=>'updated_by',
                'value'=>function($model)
                {
                    return Users::find()->where(['id'=>$model->updated_by])->one()->name;
                }
            ],
        ],
    ]) ?>


<h4> Details</h4>
<?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'layout'=>'{items}',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'job',
           //'plateno',
            //'part',
            [
                'attribute'=>'spare_part',
                'label'=>'Part',
            ],
            'quantity',
            'price',
            [
                'class' => 'yii\grid\ActionColumn',
                
                'template' => '{vprint}',
                    
                'buttons' => [
                    'vprint' => function ($url, $model, $key) {
                        $delivery= InStock::find()->where(['order_id'=>$model->order_id,'spare_part'=>$model->spare_part])->count();
                        if($delivery==0)
                        {
                        return Html::a('<button class="btn btn-warning pull-right" id="modalButton"><i>Delivery</i></button>',['/in-stock/create', 'order_id'=>$model->order_id, 'spare_part'=>$model->spare_part]
                        ,
                    );}
                        
                        
                    },
    
                ],
              ],

       ],
    ]); ?>

</div>
<br><br><br>