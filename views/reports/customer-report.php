<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use app\models\RetailsInfo;
use app\models\SpareParts;

$this->title=Yii::t('app','Customer Report');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Reports'), 'url' => ['index']];
?>

<?=GridView::widget([
  'dataProvider'=>$dataProvider,
  'summary'=>Yii::t('app','Showing {totalCount}'),
  'showPageSummary'=>true,
  'panel'=>['type'=>'info','heading'=>$this->title],
  'columns'=>[
     ['class'=>'kartik\grid\SerialColumn'],
     //'spare_part',

     [
       'label'=>'Customer',
       'attribute'=>'name',
       'format'=>'html',
       'value'=>function($model) 
       {
           return Html::a($model['name'], ['customer-details', 'retails_id'=>$model['retails_id'],'start'=>$_REQUEST['start'],'end'=>$_REQUEST['end']]);
       }
     ],
    //  'quantity',
    //  [
    //    'attribute'=>'price',
    //     'pageSummary'=>true,
    //  ],
     [
        'attribute'=>'total',
        'format'=>['decimal',2],
        'pageSummary'=>true,
      ],


    ],
  // 'rowOptions'   => function ($model, $key, $index, $grid) {
  //   return ['spare_part' => $model->spare_part];
//},


]);
?>