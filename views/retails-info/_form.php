<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

if(\Yii::$app->session->hasFlash('error')){
    echo "<b><p style='color:darkred;text-decoration:underline;'>".\Yii::$app->session->getFlash('error')."</p></b>";
}

/* @var $this yii\web\View */
/* @var $model app\models\RetailsInfo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="retails-info-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'spare_part')->textInput(['class' => 'form-control class-content-title_series','disabled' => true]) ?>

    <?= $form->field($model, 'quantity')->textInput() ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'discount')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
