<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use app\models\RetailsInfo;
use app\models\SpareParts;

$this->title=Yii::t('app','Sales Report');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sales Report'), 'url' => ['sales-report','start'=>$_REQUEST['start'],'end'=>$_REQUEST['end']]];
?>

<?=GridView::widget([
  'dataProvider'=>$dataProvider,
  'summary'=>Yii::t('app','Showing {totalCount}'),
  'showPageSummary'=>true,
  'panel'=>['type'=>'info','heading'=>$_REQUEST['spare_part']],
  'columns'=>[
     ['class'=>'kartik\grid\SerialColumn'],
     '_date',
     'amount',
     //'quantity',
     [
         'label'=>'Quantity',
         'attribute'=>'quantity',
         'pageSummary'=>'TOTAL',
     ],
    // 'Total_Amount'
    [
        'label'=>'Total Amount',
        'attribute'=>'Total_Amount',
        'pageSummary'=>true,
        'format' => ['decimal', 2],
    ],


]
])
?>