<?php


use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use sjaakp\gcharts\PieChart;
use sjaakp\gcharts\ColumnChart;


/** @var yii\web\View $this */

/*if(session_id()==null){
    $this->redirect(['index']);
}*/

$this->title = 'STOCK MANAGMENT SYSTEM';

$stock_amount=Yii::$app->db->createCommand("Select sum(quantity * price) from spare_parts")->queryScalar();
$sales=Yii::$app->db->createCommand("Select sum((price*quantity)-((price*quantity*discount)/100)) from retails_info where MONTH(from_unixTime(created_at))=month(curdate())")->queryScalar();
$spare_parts=Yii::$app->db->createCommand("select count(*) from spare_parts")->queryScalar();
$suppliers=Yii::$app->db->createCommand("select count(*) from supplier")->queryScalar();
$customers=Yii::$app->db->createCommand("select count(*) from customer")->queryScalar();
$purchase=Yii::$app->db->createCommand("select sum(quantity * price) from in_stock where month(from_unixTime(Created_at))=month(curdate())")->queryScalar();
$income=$sales-$purchase;

//SELECT spare_part, SUM(price * quantity) from retails_info GROUP BY spare_part ORDER BY sum(price*quantity) DESC LIMIT 5;
?>

<html>
<head>
  <title></title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
 <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
 
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
  <script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>

<style>
  .icon{
    font-size: 20px;
  }
  .row{
    font-weight: bold;
    font-size: 15px;
    font-family: Arial, Helvetica, sans-serif;
    text-align: center;

  }
  .small-box-footer{
    background: rgba(0, 0, 0, .1);
    text-align: center;
    border-radius: 10px;
   
  }
  .inner{
    font-weight: bold;
    font-size: 15px;
  }
  .panel-group{
    
    top:auto;
    /* display: flex; */
    /* position: fixed; */
  }

.small-box{
  border-radius:10px;
  display: flexbox;
}


</style>
</head>
<body>
<div class="site-index">

    <div class="body-content">
            <!-- top tiles -->
           
       <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row ">
          <div class="col-lg-2 col-6">
            <!-- small box -->
            <div class="small-box" style="background-color: #F9A825;color:ghostwhite;">
              <div class="inner">
              <div class="icon" >
                <i class="fa fa-cubes"></i>
              </div>
                <h2><b><?= $spare_parts?></b></h2>
                <p>Spare parts</p>
              </div>
              
              <div class="small-box-footer">
              <a href="<?=Url::to(['spare-parts/index']);?>">More info <i class="glyphicon glyphicon-circle-arrow-right"></i></a>
            </div>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-2 col-6">
            <!-- small box -->
            <div class="small-box bg-warning" style="background-color:#8C9EFF;color:ghostwhite;">
              <div class="inner">
              <div class="icon">
                <i class="fa fa-users"></i>
              </div>
                <h2><?=$suppliers?></h2>
                <p>Suppliers</p>
              </div>
              
              <div class="small-box-footer">
              <a href="<?=Url::to(['supplier/index']) ?>">More info <i class="glyphicon glyphicon-circle-arrow-right"></i></a>
            </div>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-2 col-6">
            <!-- small box -->
            <div class="small-box bg-info" style="background-color: #4DB6AC;color:ghostwhite;">
              <div class="inner">
                <div class="icon">
                <i class="fa fa-group"></i>
              </div>

                <h2><?=$customers?></h2>
                <p>Customers</p>
              </div>
              
              <div class="small-box-footer">
              <a href="<?=Url::to(['customer/index'])?>">More info <i class="glyphicon glyphicon-circle-arrow-right"></i></a>
            </div>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-2 col-6">
            <!-- small box -->
            <div class="small-box bg-success" style="background-color: #E57373;color:ghostwhite;">
              <div class="inner">
              <div class="icon">
                <i class="fa fa-bar-chart-o"></i>
              </div>
                <h2><?php echo number_format($sales,2)?></h2>
                <p>Total Sales</p>
              </div>
  
              <div class="small-box-footer">
              <a href="#" >More info <i class="glyphicon glyphicon-circle-arrow-right"></i></a>
              </div>
            </div>
          </div>
         
       <!-- ./col -->
       <div class="col-lg-2 col-6">
            <!-- small box -->
            <div class="small-box bg-success" style="background-color: #00B8D4;color:ghostwhite;">
              <div class="inner">
                <div class="icon">
                <i class="fa fa-cart-plus"></i>
              </div>

                <h2><?=number_format($purchase,2)?></h2>
                <p>Purchase</p>
              </div>
              
              <div class="small-box-footer">
              <a href="#">More info <i class="glyphicon glyphicon-circle-arrow-right"></i></a>
            </div>
            </div>
       </div>
              <!-- ./col -->

       <div class="col-lg-2 col-6">
            <!-- small box -->
            <div class="small-box bg-success"  style="background-color: #00BFA5;color:ghostwhite;">
              <div class="inner">
                <div class="icon">
                <i class="fas fa-chart-line"></i>
              </div>

                <h2><?=number_format($income,2)?></h2>
                <p>Income</p>
              </div>
              
              <div class="small-box-footer">
              <a href="#">More info <i class="glyphicon glyphicon-circle-arrow-right"></i></a>
            </div>
            </div>
          </div>
          <!-- ./col -->
    </div>

        <!-- /.row -->
  
     <!-- /top tiles -->
<br>
        
  <!-- .panel-group -->

<div class="panel-group">

  <!-- monthly sales chart -->

<div class="panel panel-default pull-right" style="width: 49%;">
    <div class="panel-body" style="width: auto;">
	
       
		<?= PieChart::widget([
    
    'dataProvider' => $dataProvider,
    'columns' => [

		'spare_part:string',
       
		['label'=>Yii::t('app','Amount'),
		 'attribute'=>'Amount',
         'format' => ['number',2] ,
		 ],
          
		//'mileage',
       
    ],
    'options' => [
        'title' => Yii::t('app', 'Top Sales')
    ],
])

 ?>
    </div>
    <br>
</div>
<!-- /. monthly income chart -->

 <!-- top sales -->
<div class="panel panel-default pull-left" style="width: 49%;">
    <div class="panel-body" style="width: auto;">
	
       
		<?= columnChart::widget([
    
    'dataProvider' => $dataProvider2,
    'columns' => [

		'_date:string',
       
		['label'=>Yii::t('app','Amount'),
		 'attribute'=>'Amount',
         'format' => ['number',2] ,
		 ],
          
		//'mileage',
       
    ],
    'options' => [
        'title' => Yii::t('app', 'Monthly Income Chart')
    ],
 ])

 ?>
 </div>
</div>

 <!-- top sales -->

  <!-- spare parts gridview -->
<div class="panel panel-default pull-left" style="width: 100%;">
    <div class="panel-body">
	
    <?= GridView::widget([
        'dataProvider' => $dataProviderSparePart,
        'filterModel' => $searchModel,
        'pjax' => true,
    'striped' => false,
    'hover' => true,
    'panel' => ['type' => 'info', 'heading' => 'Spare parts'],
    'toggleDataContainer' => ['class' => 'btn-group mr-2 me-2'],
        'summary' => Yii::t('app', 'Showing {begin}-{end} of {totalCount} Cards'),
        'columns' => [
          ['class' => 'kartik\grid\SerialColumn'],
            'name',
			      'quantity',
            
            [
              'class' => 'yii\grid\ActionColumn',
              
              'template' => '{vprint}',
                  
              'buttons' => [
                  'vprint' => function ($url, $model, $key) {
                     
                      return Html::a('<button class="btn btn-info pull-right"><i>Order</i></button>',['/orders/create','spare_part'=>$model->reference]
                      ,
                  );
                      
                      
                  },
  
              ],
            ],
            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
 </div>
</div>
<!-- /.spare parts gridview -->
</div>
<!-- /.panel-group -->
</div>
</div>
</div>
</body>
</html>