<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SpareParts */

$this->title = Yii::t('app', 'Update Spare Parts: {name}', [
    'name' => $model->name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Spare Parts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'reference' => $model->reference]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="spare-parts-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
