<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Retails;

/**
 * RetailsSearch represents the model behind the search form of `app\models\Retails`.
 */
class CustomerRetailsSearch extends RetailsInfo
{
    public $formNameParam = 'CustomerRetailsSearch';
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id','quantity', 'price', 'discount', 'created_at', 'created_by', 'updated_at', 'updated_by', 'spare_part', 'retails_id'], 'required'],
            [['id', 'quantity', 'price', 'discount', 'created_at', 'created_by', 'updated_at', 'updated_by', 'retails_id'], 'integer'],
            [['spare_part'], 'string', 'max' => 50],
            [['id'], 'unique'],
            [['spare_part'], 'exist', 'skipOnError' => true, 'targetClass' => SpareParts::className(), 'targetAttribute' => ['spare_part' => 'reference']],
            [['retails_id'], 'exist', 'skipOnError' => true, 'targetClass' => Retails::className(), 'targetAttribute' => ['retails_id' => 'retails_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RetailsInfo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            //'retails_id' => $this->retails_id,
           // 'customer' => $this->customer,
            'id'=>$this->id,
            'quantity' => $this->quantity,
            'price' => $this->price,
            'discount' => $this->discount,
            //'retails_id'=>$this->retails_id,
           
        ]);

        $query->andFilterWhere(['like', 'id', $this->id]);


        return $dataProvider;
    }
}
