<?php
namespace app\models;

use yii\base\Model;
use Yii;

class Month extends Model
{
    public $month;
    public $year;
    
    public function rules()
    {
        return [
            [['month','year'], 'required'],
            [['month','year'], 'safe']
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'month' => Yii::t('app', 'Month'),
            'year' => Yii::t('app', 'Year'),
			
        ];
    }
    public function getYearList(){
        $currentYear=date('Y');
        $yearFrom=2020;
        $yearsRange=range($yearFrom,$currentYear);
        return array_combine($yearsRange,$yearsRange);
    }
}