<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\InStock */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="in-stock-form">

    <?php $form = ActiveForm::begin(); ?>

    <!-- <?= $form->field($model, 'order_id')->textInput() ?> -->

    <?= $form->field($model, 'spare_part')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'quantity')->textInput() ?>

    <?= $form->field($model, 'price')->textInput() ?>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
