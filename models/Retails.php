<?php

namespace app\models;
use app\traits\AuditTrailsTrait;
use Yii;

/**
 * This is the model class for table "retails".
 *
 * @property int $retails_id
 * @property string $description
 * @property int $created_at
 * @property int $created_by
 * @property int $updated_at
 * @property int $updated_by
 * @property int $customer
 *
 * @property Customer $customer0
 * @property RetailsInfo[] $retailsInfos
 */
class Retails extends \yii\db\ActiveRecord
{
    use AuditTrailsTrait;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'retails';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer'], 'required'],
            [['created_at', 'created_by', 'updated_at', 'updated_by', 'customer'], 'integer'],
            [['description'], 'string', 'max' => 2000],
            [['customer'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['customer' => 'phone']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'retails_id' => Yii::t('app', 'Retails ID'),
            'description' => Yii::t('app', 'Description'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'customer' => Yii::t('app', 'Customer'),
        ];
    }

    /**
     * Gets query for [[Customer0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer0()
    {
        return $this->hasOne(Customer::className(), ['phone' => 'customer']);
    }

    /**
     * Gets query for [[RetailsInfos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRetailsInfos()
    {
        return $this->hasMany(RetailsInfo::className(), ['retails_id' => 'retails_id']);
    }
}
