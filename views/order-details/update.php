<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\OrderDetails */

$this->title = Yii::t('app', 'Update Order Details: {name}', [
    'name' => $model->order_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Order Details'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->order_id, 'url' => ['view', 'order_id' => $model->order_id, 'spare_part' => $model->spare_part]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="order-details-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
