<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\FindReport */
/* @var $form ActiveForm */
?>
<div class="reports-_reportsForm">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'report')->dropDownList([
            'sales-report'=>Yii::t('app', 'Sales Report'),
			'purchase-report'=>Yii::t('app', 'Purchase report'),
            'customer-report'=>Yii::t('app', 'Customer report'),
            'spare-part-report'=>Yii::t('app', 'Spare Part report'),
            
	
        ]) ?>

        <div class='row'>
            <div class='col-md-6'>
            <?= $form->field($model, 'start')->widget(DatePicker::classname(), [
                    'options' => ['placeholder' => 'Enter return date ...'],
                    'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                      ]
                 ]); 
                ?>
            </div>
            <div class='col-md-6'>
                <?= $form->field($model, 'end')->widget(DatePicker::classname(), [
                    'options' => ['placeholder' => 'Enter return date ...'],
                    'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                      ]
                 ]); 
                ?>
            </div>
        </div>    
        
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Find'), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- reports-_findReport -->
