<?php

namespace app\controllers;

use app\models\SpareParts;
use app\models\SparePartsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SparePartsController implements the CRUD actions for SpareParts model.
 */
class SparePartsController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all SpareParts models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new SparePartsSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SpareParts model.
     * @param string $reference Reference
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($reference)
    {
        return $this->render('view', [
            'model' => $this->findModel($reference),
        ]);
    }

    /**
     * Creates a new SpareParts model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new SpareParts();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'reference' => $model->reference]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing SpareParts model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $reference Reference
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($reference)
    {
        $model = $this->findModel($reference);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'reference' => $model->reference]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing SpareParts model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $reference Reference
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($reference)
    {
        $this->findModel($reference)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SpareParts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $reference Reference
     * @return SpareParts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($reference)
    {
        if (($model = SpareParts::findOne(['reference' => $reference])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(\Yii::t('app', 'The requested page does not exist.'));
    }
}
