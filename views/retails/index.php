<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RetailsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Retails');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="retails-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Retails'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'retails_id',
            'description',
            // 'created_at',
            // 'created_by',
            // 'updated_at',
            //'updated_by',
            'customer',
            // [
            //     'class' => ActionColumn::className(),
            //     'urlCreator' => function ($action, app\models\Retails $model, $key, $index, $column) {
            //         return Url::toRoute([$action, 'retails_id' => $model->retails_id]);
            //      }
            // ],
            [
                'class' => ActionColumn::className(),
            
                'template' => '{view}',  // the default buttons + your custom button
            'urlCreator' => function ($action, app\models\Retails $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'retails_id' => $model->retails_id]);
                 }
            
        ],
        ],
    ]); ?>


</div>
