<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use app\models\RetailsInfo;
use app\models\SpareParts;

$this->title=Yii::t('app','Sales Report');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Report'), 'url' => ['index']];
?>

<?=GridView::widget([
  'dataProvider'=>$dataProvider,
  'summary'=>Yii::t('app','Showing {totalCount}'),
  'showPageSummary'=>true,
  'panel'=>['type'=>'info','heading'=>$this->title],
  'columns'=>[
     ['class'=>'kartik\grid\SerialColumn'],
     //'spare_part',

     [
       'label'=>'Spare Part',
       'attribute'=>'spare_part',
       'format'=>'html',
       'value'=>function($model) 
       {
           return Html::a($model['spare_part'], ['sales-details', 'spare_part'=>$model['spare_part'],'start'=>$_REQUEST['start'],'end'=>$_REQUEST['end']]);
       }
     ],
     'Quantity',
     [
       'attribute'=>'Amount',
       'format'=>['decimal',2],
       'pageSummary'=>true,
     ],


    ],
  // 'rowOptions'   => function ($model, $key, $index, $grid) {
  //   return ['spare_part' => $model->spare_part];
//},


]);
?>