<?php

namespace app\controllers;

use app\models\Customer;
use app\models\DocumentReport;
use app\models\InStock;
use app\models\Month;
use app\models\Report;
use app\models\Retails;
use app\models\RetailsInfo;
use app\models\RetailsInfoSearch;
use Behat\Gherkin\Loader\YamlFileLoader;
use SebastianBergmann\CodeCoverage\Report\Html\Renderer;
use Yii;
use yii\data\SqlDataProvider;
use yii\base\DynamicModel;
use yii\helpers\ArrayHelper;

class ReportsController extends \yii\web\Controller
{
       
    public function actionIndex(){
        $model=new  \app\models\Report();
        if($model->load(Yii::$app->request->post())){
          
            if($model->validate()){
                return $this->redirect([$model->report,'start'=>$model->start,'end'=>$model->end]);
            }
        }
        return $this->render('index',['model'=>$model]);

    }
    public function actionSalesReport($start,$end){
        $model=new RetailsInfo();
        $sql="select spare_part,sum(quantity) as Quantity,sum((price*quantity)-((price*quantity*discount)/100)) as Amount from retails_info where date(from_unixTime(created_at)) between :start and :end Group by spare_part";
        $params=[':start'=>$start,':end'=>$end];

        $counter=Yii::$app->db->createCommand("select count(*) from ($sql) t",$params)->queryScalar();

        $dataProvider= new SqlDataProvider([

            'sql'=>$sql,
            'totalCount'=>$counter,
            'params'=>$params
        ]);

        return $this->render('sales-report',[
            'dataProvider'=>$dataProvider,
            'model'=>$model,
        ]);
    }

    public function actionSalesDetails($spare_part,$start,$end){
        $model=new RetailsInfo();
        $sql="select date(from_unixTime(created_at)) as _date,quantity,price as amount,sum((price*quantity)-((price*quantity*discount)/100)) Total_Amount from retails_info where date(from_unixTime(created_at)) between :start and :end and spare_part=:spare_part ";
        $params=[':start'=>$start,':end'=>$end,':spare_part'=>$spare_part];

        $counter=Yii::$app->db->createCommand("select count(*) from ($sql) t",$params)->queryScalar();

        $dataProvider= new SqlDataProvider([

            'sql'=>$sql,
            'totalCount'=>$counter,
            'params'=>$params
        ]);

        return $this->render('sales-details',[
            'dataProvider'=>$dataProvider,
            'model'=>$model,
        ]);
    }

    public function actionPurchaseReport($start,$end){
        $model=new InStock();
        $sql="select spare_part,sum(quantity) as Quantity,sum(price*quantity) as Amount from in_stock where date(from_unixTime(created_at)) between :start and :end Group by spare_part";
        $params=[':start'=>$start,':end'=>$end];

        $counter=Yii::$app->db->createCommand("select count(*) from ($sql) t",$params)->queryScalar();

        $dataProvider= new SqlDataProvider([

            'sql'=>$sql,
            'totalCount'=>$counter,
            'params'=>$params
        ]);

        return $this->render('purchase-report',[
            'dataProvider'=>$dataProvider,
            'model'=>$model,
        ]);
    }

    public function actionPurchaseDetails($spare_part,$start,$end){
        $model=new InStock();
        $sql="select date(from_unixTime(created_at)) as _date,quantity,price as amount,(price*quantity) as Total_Amount from in_stock where date(from_unixTime(created_at)) between :start and :end and spare_part=:spare_part ";
        $params=[':start'=>$start,':end'=>$end,':spare_part'=>$spare_part];

        $counter=Yii::$app->db->createCommand("select count(*) from ($sql) t",$params)->queryScalar();

        $dataProvider= new SqlDataProvider([

            'sql'=>$sql,
            'totalCount'=>$counter,
            'params'=>$params
        ]);

        return $this->render('purchase-details',[
            'dataProvider'=>$dataProvider,
            'model'=>$model,
        ]);
    }

    public function actionCustomerReport($start,$end){
        $model=new RetailsInfo();

        
        $sql="SELECT c.name,ri.spare_part,sum(ri.quantity) as quantity,sum(ri.price)as price,sum((ri.price*ri.quantity)-((ri.price*ri.quantity*ri.discount)/100)) as total,ri.retails_id FROM customer c,retails_info ri,retails r WHERE c.phone=r.customer and r.retails_id=ri.retails_id and date(from_unixTime(ri.created_at)) between :start and :end GROUP BY c.name ";
        $params=[':start'=>$start,':end'=>$end];

        $counter=Yii::$app->db->createCommand("select count(*) from ($sql) t",$params)->queryScalar();

        $dataProvider= new SqlDataProvider([

            'sql'=>$sql,
            'totalCount'=>$counter,
            'params'=>$params
        ]);

        return $this->render('customer-report',[
            'dataProvider'=>$dataProvider,
            'model'=>$model,
        ]);
    }

    public function actionCustomerDetails($retails_id,$start,$end){

        $model=new RetailsInfo();

        $cust=Retails::find()->where(['retails_id'=>$retails_id])->one()->customer;
        $customer_name=Customer::find()->where(['phone'=>$cust])->one()->name;
        
        $sql="select distinct date(from_unixTime(ri.created_at)) as _date,ri.spare_part,ri.quantity,ri.discount,ri.price as amount,((price*quantity)-((price*quantity*discount)/100)) as Total_Amount from customer c,retails_info ri,retails r WHERE r.customer=:cust and r.retails_id=ri.retails_id and date(from_unixTime(ri.created_at)) between :start and :end ";
        $params=[':start'=>$start,':end'=>$end,':cust'=>$cust];

        $counter=Yii::$app->db->createCommand("select count(*) from ($sql) t",$params)->queryScalar();

        $dataProvider= new SqlDataProvider([

            'sql'=>$sql,
            'totalCount'=>$counter,
            'params'=>$params
        ]);

        return $this->render('customer_details',[
            'dataProvider'=>$dataProvider,
            'model'=>$model,
        ]);
    }

    public function actionSparePartReport($start,$end){
        $model=new RetailsInfo();

        
        //$sql="SELECT distinct r.spare_part,date(from_unixTime(r.created_at)) as _date,i.quantity as purchase,r.quantity as sales from in_stock i,retails_info r WHERE i.spare_part=r.spare_part  and date(from_unixTime(r.created_at)) between :start and :end  ";
          $sql="SELECT _date,spare_part,sum(sin) as purchase,sum(sout) as sales FROM (
            SELECT date(from_unixtime(created_at)) as _date,spare_part,quantity as sin,0 as sout FROM stock.in_stock union
            SELECT date(from_unixtime(created_at)) as _date,spare_part,0 as sin,quantity as sout FROM stock.retails_info)t join stock.spare_parts s on t.spare_part=s.reference where _date between :start and :end  group by _date,spare_part ";
        $params=[':start'=>$start,':end'=>$end];

        $counter=Yii::$app->db->createCommand("select count(*) from ($sql) t",$params)->queryScalar();

        $dataProvider= new SqlDataProvider([

            'sql'=>$sql,
            'totalCount'=>$counter,
            'params'=>$params
        ]);

        return $this->render('spare-part-report',[
            'dataProvider'=>$dataProvider,
            'model'=>$model,
        ]);
    }

    public function actionMonthlyReport(){
       $model=new Month();
      
    if ($model->load(Yii::$app->request->post())) {
      
        $month=$model->month;
        $year=$model->year;
        

        $sql="SELECT s.reference,(s.quantity-sum(i.quantity)+sum(o.quantity)) as quantity,s.price from spare_parts s,in_stock i,retails_info o where s.reference=i.spare_part and s.reference=o.spare_part and month(from_unixTime(i.created_at))=:m and year(from_unixTime(i.created_at))=:y group by s.reference ";
        
        $params=[':m'=>$month,':y'=>$year];

        $counter=Yii::$app->db->createCommand("select count(*) from ($sql) t",$params)->queryScalar();

        $dataProvider= new SqlDataProvider([

            'sql'=>$sql,
            'totalCount'=>$counter,
            'params'=>$params
        ]);

        return $this->render('monthly-report',[
            'dataProvider'=>$dataProvider,
            'model'=>$model,
        ]);

  
    }
    $current_Year=date('Y');
    //var_dump($current_Year);

        $sql="SELECT s.reference,(s.quantity-sum(i.quantity)+sum(o.quantity)) as quantity,s.price from spare_parts s,in_stock i,retails_info o where s.reference=i.spare_part and s.reference=o.spare_part and year(from_unixTime(i.created_at))= :y OR year(from_unixTime(o.created_at))= :y group by s.reference ";
        
        $params=[':y'=>$current_Year];

        $counter=Yii::$app->db->createCommand("select count(*) from ($sql) t",$params)->queryScalar();

        $dataProvider= new SqlDataProvider([

            'sql'=>$sql,
            'totalCount'=>$counter,
            'params'=>$params
        ]);

        return $this->render('monthly-report',[
            'dataProvider'=>$dataProvider,
            'model'=>$model,
        ]);

     
    }
        
        
    


}

?>