-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 12, 2022 at 04:33 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `stock`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `phone` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `created_at` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`phone`, `name`, `address`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(789654488, 'maombe', 'Kigali,Nyarugenge', 1649768695, 1, 1649768695, 1);

-- --------------------------------------------------------

--
-- Table structure for table `in_stock`
--

CREATE TABLE `in_stock` (
  `quantity` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `spare_part` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `in_stock`
--

INSERT INTO `in_stock` (`quantity`, `price`, `created_at`, `created_by`, `updated_at`, `updated_by`, `order_id`, `spare_part`) VALUES
(100, 10000, 1649767952, 1, 1649769430, 1, 2, 'Wheels'),
(300, 20000, 1649769360, 1, 1649769360, 1, 3, 'Wheels'),
(100, 12000, 1649771387, 1, 1649771387, 1, 4, 'Water pump'),
(100, 10000, 1649769612, 1, 1649769612, 1, 4, 'Wheels');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `description` varchar(100) NOT NULL,
  `status` varchar(20) NOT NULL,
  `created_at` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `supplier` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `date`, `description`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`, `supplier`) VALUES
(1, '2022-01-18', 'order5', '1', 1649765843, 1, 1649765843, 1, 'Aristide'),
(2, '2022-01-18', 'order5', '1', 1649766989, 1, 1649766989, 1, 'Aristide'),
(3, '2022-01-18', 'order', '1', 1649769130, 1, 1649769130, 1, 'Aristide'),
(4, '2022-01-18', 'order4', '1', 1649769591, 1, 1649769591, 1, 'Aristide');

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `quantity` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `spare_part` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`quantity`, `price`, `created_at`, `created_by`, `updated_at`, `updated_by`, `order_id`, `spare_part`) VALUES
(100, 15000, 1649767079, 1, 1649767079, 1, 1, 'Wheels'),
(100, 10000, 1649767936, 1, 1649767936, 1, 2, 'Wheels'),
(300, 20000, 1649769157, 1, 1649769157, 1, 3, 'Wheels'),
(100, 1000, 1649771368, 1, 1649771368, 1, 4, 'Water pump'),
(100, 10000, 1649769607, 1, 1649769607, 1, 4, 'Wheels');

-- --------------------------------------------------------

--
-- Table structure for table `retails`
--

CREATE TABLE `retails` (
  `retails_id` int(11) NOT NULL,
  `description` varchar(2000) NOT NULL,
  `created_at` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `customer` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `retails`
--

INSERT INTO `retails` (`retails_id`, `description`, `created_at`, `created_by`, `updated_at`, `updated_by`, `customer`) VALUES
(1, 'order0', 1649770121, 1, 1649770121, 1, 789654488),
(2, 'order0', 1649770426, 1, 1649770426, 1, 789654488),
(3, 'order1', 1649771430, 1, 1649772882, 1, 789654488);

-- --------------------------------------------------------

--
-- Table structure for table `retails_info`
--

CREATE TABLE `retails_info` (
  `id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `discount` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `spare_part` varchar(50) NOT NULL,
  `retails_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `retails_info`
--

INSERT INTO `retails_info` (`id`, `quantity`, `price`, `discount`, `created_at`, `created_by`, `updated_at`, `updated_by`, `spare_part`, `retails_id`) VALUES
(1, 100, 400, 10, 1649770470, 1, 1649770470, 1, 'Wheels', 2),
(3, 60, 12000, 0, 1649771784, 1, 1649771784, 1, 'Water pump', 3),
(4, 100, 345, 10, 1649772912, 1, 1649772912, 1, 'Wheels', 3);

-- --------------------------------------------------------

--
-- Table structure for table `spare_parts`
--

CREATE TABLE `spare_parts` (
  `reference` varchar(50) NOT NULL,
  `name` varchar(60) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `min_stock` int(11) NOT NULL,
  `unit` varchar(50) NOT NULL,
  `created_at` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `spare_parts`
--

INSERT INTO `spare_parts` (`reference`, `name`, `quantity`, `price`, `min_stock`, `unit`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
('Water pump', 'Water pump', 40, 12000, 5, 'piece', 1649771339, 1, 1649771339, 1),
('Wheels', 'wheels', 200, 345, 5, 'piece', 1649767038, 1, 1649769327, 1);

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `name` varchar(100) NOT NULL,
  `tin` int(11) NOT NULL,
  `address` varchar(100) NOT NULL,
  `phone` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`name`, `tin`, `address`, `phone`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
('Aristide', 123, 'Muhanga', 785689909, 1649765148, 1, 1649765148, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `phone` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `authKey` varchar(200) NOT NULL,
  `created_at` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `phone`, `username`, `password`, `authKey`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'louange', 78546757, 'louange', '1234', '', 0, 0, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`phone`);

--
-- Indexes for table `in_stock`
--
ALTER TABLE `in_stock`
  ADD PRIMARY KEY (`order_id`,`spare_part`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `supplier` (`supplier`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`order_id`,`spare_part`),
  ADD KEY `spare_part` (`spare_part`);

--
-- Indexes for table `retails`
--
ALTER TABLE `retails`
  ADD PRIMARY KEY (`retails_id`),
  ADD KEY `customer` (`customer`);

--
-- Indexes for table `retails_info`
--
ALTER TABLE `retails_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `spare_part` (`spare_part`),
  ADD KEY `retails_id` (`retails_id`);

--
-- Indexes for table `spare_parts`
--
ALTER TABLE `spare_parts`
  ADD PRIMARY KEY (`reference`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `retails`
--
ALTER TABLE `retails`
  MODIFY `retails_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `retails_info`
--
ALTER TABLE `retails_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `in_stock`
--
ALTER TABLE `in_stock`
  ADD CONSTRAINT `in_stock_ibfk_1` FOREIGN KEY (`order_id`,`spare_part`) REFERENCES `order_details` (`order_id`, `spare_part`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`supplier`) REFERENCES `supplier` (`name`);

--
-- Constraints for table `order_details`
--
ALTER TABLE `order_details`
  ADD CONSTRAINT `order_details_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`),
  ADD CONSTRAINT `order_details_ibfk_2` FOREIGN KEY (`spare_part`) REFERENCES `spare_parts` (`reference`);

--
-- Constraints for table `retails`
--
ALTER TABLE `retails`
  ADD CONSTRAINT `retails_ibfk_1` FOREIGN KEY (`customer`) REFERENCES `customer` (`phone`);

--
-- Constraints for table `retails_info`
--
ALTER TABLE `retails_info`
  ADD CONSTRAINT `retails_info_ibfk_1` FOREIGN KEY (`spare_part`) REFERENCES `spare_parts` (`reference`),
  ADD CONSTRAINT `retails_info_ibfk_2` FOREIGN KEY (`retails_id`) REFERENCES `retails` (`retails_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
