<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use app\models\Retails;
use app\models\Customer;

$this->title=Yii::t('app','Customer Details');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Customer Report'), 'url' => ['customer-report','start'=>$_REQUEST['start'],'end'=>$_REQUEST['end']]];
$retails_id=$_REQUEST['retails_id'];

$cust=Retails::find()->where(['retails_id'=>$retails_id])->one()->customer;
$customer_name=Customer::find()->where(['phone'=>$cust])->one()->name;
?>

<?=GridView::widget([
  'dataProvider'=>$dataProvider,
  'summary'=>Yii::t('app','Showing {totalCount}'),
  'showPageSummary'=>true,
  'panel'=>['type'=>'info','heading'=>$customer_name],
  'columns'=>[
     ['class'=>'kartik\grid\SerialColumn'],
     '_date',
     'spare_part',
     'amount',
     
     [
         'label'=>'Quantity',
         'attribute'=>'quantity',
         'pageSummary'=>'TOTAL',
     ],
    'discount',
    [
        'label'=>'Total Amount',
        'attribute'=>'Total_Amount',
        'pageSummary'=>true,
        'format' => ['decimal', 2],
    ],


]
])
?>